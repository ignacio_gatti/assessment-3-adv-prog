﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace Assessment_3_Adv_Prog
{
    public partial class EditingPage : ContentPage
    {
        private SQLiteAsyncConnection _connection;
        private ObservableCollection<WebURL> _url;
        private int selectedNumber;

        public EditingPage(WebURL selectedClass, int selection)
        {
            InitializeComponent();
            BindingContext = selectedClass;
            selectedNumber = selectID;

        }

        async void URLEditing(object sender, EventArgs e)
        {
            var url = await _connection.Table<WebURL>().ToListAsync();
            _url = new ObservableCollection<WebURL>(url);

            var urlBlock = url[selectedNumber];
            urlBlock.Title = TitleNew.Text;
            urlBlock.URL = URLNew.Text;
            urlBlock.ImageShow = ImageNew.Text;

            await _connection.UpdateAsync(urlBlock);
        }


    }
}
